import React, { useState, useEffect } from 'react';
import api from '~/services/api';

import {
  Background,
  Container,
  Title,
  EnterprisesList,
  Enterprise,
  EnterpriseTitle,
} from './styles';

export default function Home() {
  const [enterprises, setEnterprises] = useState([]);

  useEffect(() => {
    async function loadEnterprises() {
      const response = await api.get('enterprises', {
        headers: {
          'access-token': 'IDzuxLMx6-La6DrfAPtWNg',
          client: 'GtViaX_TA_J3of49GNjhQA',
          uid: 'testeapple@ioasys.com.br',
        },
      });

      setEnterprises(response.data.enterprises);
    }

    loadEnterprises();
  }, []);

  return (
    <Background>
      <Container>
        <Title>Empresas</Title>

        <EnterprisesList
          data={enterprises}
          keyExtractor={enterprise => String(enterprise.id)}
          renderItem={({ item }) => (
            <Enterprise>
              <EnterpriseTitle>{item.enterprise_name}</EnterpriseTitle>
            </Enterprise>
          )}
        />
      </Container>
    </Background>
  );
}
