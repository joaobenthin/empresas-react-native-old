import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';

export const Background = styled(LinearGradient).attrs({
  colors: ['#e51f6b', '#000'],
})`
  flex: 1;
`;

export const Container = styled.View`
  flex: 1;
`;

export const Title = styled.Text`
  font-size: 20px;
  color: #fff;
  font-weight: bold;
  align-self: center;
  margin-top: 30px;
`;

export const EnterprisesList = styled.FlatList``;

export const Enterprise = styled.View`
  flex: 1;
  margin: 10px 20px 0;
  padding-left: 20px;
  background: #fff;
  border-radius: 4px;
  height: 70px;

  justify-content: center;
`;

export const EnterpriseTitle = styled.Text`
  font-size: 18px;
  color: #000;
`;
