import { takeLatest, call, put, all } from 'redux-saga/effects';

import api from '~/services/api';

import { signInSuccess } from './actions';

export function* signIn({ payload }) {
  const { email, password } = payload;

  const response = yield call(api.post, 'users/auth/sign_in', {
    email,
    password,
  });

  const { access_token, client, uid } = response;

  yield put(signInSuccess(access_token, client, uid));
}

export default all([takeLatest('@auth/SIGN_IN_REQUEST', signIn)]);
