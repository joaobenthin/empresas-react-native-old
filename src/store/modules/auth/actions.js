export function signInRequest(email, password) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: { email, password },
  };
}

export function signInSuccess(access_token, client, uid, user) {
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    payload: { access_token, client, uid, user },
  };
}

export function signFailure() {
  return {
    type: '@auth/SIGN_FAILURE',
  };
}
